all: demo-coap

CONTIKI=../../os/contiki
CFLAGS += -DPROJECT_CONF_H=\"project-conf.h\"

# linker optimizations
SMALL=1

# REST framework, requires WITH_COAP
${info INFO: compiling with CoAP-07}
CFLAGS += -DWITH_COAP=7
CFLAGS += -DREST=coap_rest_implementation
CFLAGS += -DUIP_CONF_IPV6=1
APPS += er-coap-07
APPS += erbium

MY_APPS += spi_driver spi_flash tiki_utils twi_driver twi_78M6610

APPS += $(MY_APPS) hmd_relay_process
APPDIRS += $(join $(addprefix ../../apps/,$(MY_APPS)), )

include $(CONTIKI)/Makefile.include

