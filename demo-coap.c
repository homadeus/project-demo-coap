/*
 * Homadeus Platform COAP Demo
 * Copyright (C) 2013 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <contiki.h>
#include <contiki-net.h>
#include <settings.h>

#include <erbium.h>
#include <er-coap-07.h>

#include <homadeus/processes/relay.h>
#include <homadeus/devices/78M6610.h>

#define DEBUG 1
#include <homadeus/utils/debug.h>

PROCESS(coap_demo, "Rest Server Example");
PROCESS(blink_process, "Blink Process Example");

AUTOSTART_PROCESSES(&coap_demo, &hmd_relay_process, &blink_process);

PROCESS_THREAD(blink_process, ev, data)
{
    static struct etimer timer;
    static uint8_t relay_state = 0;

    PROCESS_BEGIN();

    hmd_relay_event = process_alloc_event();
    etimer_set(&timer, CLOCK_CONF_SECOND * 5);

    while (1)
    {
        // we set the timer from here every time
        // and wait until the vent we receive is the one we're waiting for
        PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
        process_post(&HOMADEUS_RELAY_PROCESS, HOMADEUS_RELAY_EVENT, &relay_state);
        etimer_reset(&timer);
        relay_state = !relay_state;
    }

    PROCESS_END();
}

PROCESS_THREAD(coap_demo, ev, data)
{
  PROCESS_BEGIN();

  PRINTF("Rest Example\n");

  maxim_78M6610_debug_version();

#ifdef RF_CHANNEL
  PRINTF("RF channel: %u\n", RF_CHANNEL);
#endif
#ifdef IEEE802154_PANID
  PRINTF("PAN ID: 0x%04X\n", IEEE802154_PANID);
#endif

  PRINTF("uIP buffer: %u\n", UIP_BUFSIZE);
  PRINTF("LL header: %u\n", UIP_LLH_LEN);
  PRINTF("IP+UDP header: %u\n", UIP_IPUDPH_LEN);
  PRINTF("REST max chunk: %u\n", REST_MAX_CHUNK_SIZE);

  /* Initialize the REST framework. */
  rest_init_engine();

  PROCESS_END();
}
